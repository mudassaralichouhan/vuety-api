<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('images/', function (\Illuminate\Http\Request $request) {
    $path = storage_path('app/' . (ltrim($request->path, '/')));

    if (!File::exists($path)) {
        abort(404);
    }

    $response = Response::make(File::get($path));
    $response->header("Content-Type", File::mimeType($path));

    return $response;
})->name('storage.image');
