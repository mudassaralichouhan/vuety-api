<?php

use App\Http\Controllers\EarthController;

Route::controller(EarthController::class)
    ->middleware(['throttle:30,1'])
    ->group(function () {
        Route::get('countries', 'countries');
        Route::get('countries/{id}/cities', 'cities');
        Route::get('countries/{id}/states', 'states');

        Route::get('city/{id}/country', 'city');
        Route::get('city/{id}/state', 'city');

        Route::get('country/{country}', 'country');
        Route::get('state/{state}', 'state');
        Route::get('city/{city}', 'city');
    });
