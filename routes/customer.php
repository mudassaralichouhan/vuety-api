<?php

use App\Http\Controllers\Customer\Auth\EmailController;
use App\Http\Controllers\Customer\Auth\LoginController;
use App\Http\Controllers\Customer\Auth\ResetPasswordController;
use App\Http\Controllers\Customer\Auth\VerificationController;
use App\Http\Controllers\User\UserMetaController;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(callback: function () {

    Route::middleware(['throttle:15,1', 'guest:sanctum'])
        ->prefix('password')
        ->controller(ResetPasswordController::class)
        ->group(function () {
            Route::post('forgot', 'forgot')->name('password.email');
            Route::post('reset/{token}', 'reset')->name('password.reset');
            Route::get('forgot', 'redirectToWeb')->name('password.reset');
        });

    Route::controller(LoginController::class)
        ->group(function () {

            Route::post('register', 'register');
            Route::post('login', 'login')->name('name');

            Route::middleware('auth:sanctum')->group(function () {
                Route::get('/', 'auth');
                Route::post('/logout', 'logout');
            });
        });

    Route::controller(VerificationController::class)
        ->middleware(['throttle:15,1'])
        ->prefix('email')
        ->group(function () {
            Route::get('verify/{id}', 'verify')->name('verification.verify');
            Route::get('verified/{user}', 'verified')->name('verification.verified');
            Route::post('resend', 'resend')->name('verification.resend');
            Route::get('notice', 'notice')->name('verification.notice');
        });
});

Route::middleware('auth:sanctum')
    ->group(function () {
        Route::controller(UserMetaController::class)
            ->group(function () {
                Route::get('profile', 'show');
                Route::put('profile', 'update');
                Route::patch('password/change', 'passwordChange');
                Route::post('photo', 'photo');
            });
        Route::controller(EmailController::class)
            ->prefix('email')
            ->group(function () {
                Route::get('', 'index');
                Route::post('', 'store');
                Route::delete('{id}', 'destroy');

                Route::middleware('throttle:3,1')
                    ->prefix('send-verification-token')
                    ->group(function () {
                        Route::patch('make-secondary', 'sendVerificationTokenMakeSecondary');
                        Route::patch('make-primary', 'sendVerificationTokenMakePrimary');
                    });
                Route::prefix('verify-token')
                    ->withoutMiddleware('auth:sanctum')
                    ->group(function () {
                        Route::get('{token}/secondary', 'verifyTokenSecondary')->name('email.make-secondary');
                        Route::get('{token}/primary', 'verifyTokenPrimary')->name('email.make-primary');
                    });
            });

        Route::prefix('gallery')
            ->group(function () {

                Route::controller(\App\Http\Controllers\Customer\AlbumController::class)
                    ->prefix('albums')
                    ->group(function () {
                        Route::get('', 'index');
                        Route::get('{id}', 'show');
                        Route::post('', 'updateOrStore');
                        Route::post('{id}/attach-photo', 'attachPhoto');
                        Route::delete('{id}/photo/{photo_id}', 'removePhoto');
                        Route::delete('{id}', 'destroy');
                    });

                Route::controller(\App\Http\Controllers\Customer\MediaController::class)
                    ->prefix('photos')
                    ->group(function () {
                        Route::get('', 'index');
                        Route::get('{id}', 'show');
                        Route::post('', 'store');
                        Route::post('{photos}/favorite', 'attachWithFavorite');
                        Route::delete('{id}', 'destroy');
                        Route::post('{id}/restore', 'restore');
                        Route::delete('{id}/destroy-force', 'destroyForce');
                        Route::get('trash/list', 'trashList');
                        Route::delete('trash/empty', 'trashEmpty');
                    });
            });
    });
