<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    protected $levels = [];

    protected $dontReport = [];

    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function register(): void
    {
        $this->renderable(function (Throwable $e, $request) {
            $code = intval($e->getCode());

            if ($code <=200 || $code >= 500)
                $code = 500;

            if ($request->is('api/*')) {
                return Response::json([
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ], $code);
            }
        });
    }
}
