<?php

namespace App\Console;

use App\Console\Commands\RemoveOldPersonalAccessTokensCommand;
use Illuminate\Console\Scheduling\Schedule;

class Kernel extends \Illuminate\Foundation\Console\Kernel
{
    protected $commands = [
        RemoveOldPersonalAccessTokensCommand::class
    ];
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('sanctum:prune-expired --hours=24')->days(1);
        $schedule->command('app:remove-old-personal-access-tokens-command')->weekly();
    }

    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
