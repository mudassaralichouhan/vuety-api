<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RemoveOldPersonalAccessTokensCommand extends Command
{
    protected $signature = 'app:remove-old-personal-access-tokens-command';

    protected $description = 'remove old tokens';

    public function handle()
    {
        \DB::table('personal_access_tokens')
            ->whereDate('created_at', '<', now()->subDays(7))
            ->delete();
    }
}
