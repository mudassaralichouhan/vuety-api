<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'paragraph',
        'status',
        'thumbnail',
    ];

    protected function thumbnail(): Attribute
    {
        return Attribute::make(
            get: function (string $value) {
                return ['original' => $value, 'path' => route('storage.image', ['path' => $value])];
            },
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function medias()
    {
        return $this->belongsToMany(Media::class, 'album_has_photos');
    }
}
