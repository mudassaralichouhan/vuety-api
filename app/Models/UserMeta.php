<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company_name',
        'address',
        'city_id',
        'postal_code',
        'state_id',
        'country_id',
        'bio',
        'about_me',
        'website_url',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
