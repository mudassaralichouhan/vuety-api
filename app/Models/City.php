<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public function country()
    {
        return $this->hasOneThrough(Country::class, self::class, 'id', 'id', 'id', 'country_id');
    }

    public function state()
    {
        return $this->hasOneThrough(State::class, self::class, 'id', 'id', 'id', 'state_id');
    }
}
