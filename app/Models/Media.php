<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'gallery_id',
        'description',
        'file',
        'mime',
        'thumb_path',
        'favorite',
        'status',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];
    protected $dates = ['deleted_at'];

    protected function file(): Attribute
    {
        return Attribute::make(
            get: function (string $value) {
                return ['original' => $value, 'path' => route('storage.image', ['path' => $value])];
            },
        );
    }

    public function albums()
    {
        return $this->belongsToMany(Album::class, 'album_has_photos');
    }
}
