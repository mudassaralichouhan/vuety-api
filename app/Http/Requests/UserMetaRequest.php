<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserMetaRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'company_name' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'city_id' => 'nullable|exists:cities,id',
            'postal_code_id' => 'nullable|exists:postal_codes,id',
            'country_id' => 'nullable|exists:countries,id',
            'bio' => 'nullable|max:255|string',
            'about_me' => 'nullable|max:255',
            'website_url' => 'nullable|url|max:200',
            'status' => [
                'nullable',
                Rule::in([1, 0]),
            ],
        ];
    }
}
