<?php

namespace App\Http\Requests;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'string|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required|min:6|max:50',
        ];
    }
}
