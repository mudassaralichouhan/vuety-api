<?php

namespace App\Http\Requests;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'string|required|min:6|max:50',
        ];
    }
}
