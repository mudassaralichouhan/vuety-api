<?php

namespace App\Http\Requests;

class GalleryPhotosRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'description' => 'nullable|string|max:255',
            'files.*' => [
                'required',
                'file',
                'mimetypes:image/jpeg,image/png,image/jpg,audio/mpeg,video/mp4',
                function ($attribute, $value, $fail) {
                    $max_size = [
                        'image' => 10 * 1024 * 1024, // 10MB
                        'audio' => 7 * 1024 * 1024,  // 7MB
                        'video' => 100 * 1024 * 1024 // 100MB
                    ];

                    $fileType = $value->getMimeType();
                    $fileSize = $value->getSize();

                    if (strpos($fileType, 'image/') === 0 && $fileSize > $max_size['image']) {
                        $fail('The '.$attribute.' must be a maximum of '.$max_size['image'].' kilobytes for image files.');
                    } elseif (strpos($fileType, 'audio/') === 0 && $fileSize > $max_size['audio']) {
                        $fail('The '.$attribute.' must be a maximum of '.$max_size['audio'].' kilobytes for audio files.');
                    } elseif (strpos($fileType, 'video/') === 0 && $fileSize > $max_size['video']) {
                        $fail('The '.$attribute.' must be a maximum of '.$max_size['video'].' kilobytes for video files.'.$fileSize);
                    }
                },
            ],
        ];
    }

}
