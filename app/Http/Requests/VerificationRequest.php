<?php

namespace App\Http\Requests;

class VerificationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|max:255|email|unique:users,email',
        ];
    }
}
