<?php

namespace App\Http\Requests;

class AlbumRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            //'title' => 'required|string|max:100|unique:galleries,title,NULL,id,user_id,1',
            'title' => 'required|string|max:100',
            'paragraph' => 'nullable|string|max:255',
            'thumbnail' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'image_url' => 'nullable|url|max:255',
        ];
    }
}
