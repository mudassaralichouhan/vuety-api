<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest as HttpFormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseMessage;

class FormRequest extends HttpFormRequest
{
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            Response::json($validator->errors(), ResponseMessage::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
