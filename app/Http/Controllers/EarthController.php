<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\Response;

class EarthController extends Controller
{
    public function countries()
    {
        return Response::json(Country::select(['id', 'name'])->get());
    }

    public function cities(int $country)
    {
        $cities = City::select([
            'id',
            'name',
            'state_id',
            'state_code',
            'latitude',
            'longitude',
        ])->where(['country_id' => $country])->get();

//        $country->states;
//        $country->cities;

        return Response::json($cities);
    }

    public function states(int $country)
    {
        $state = State::select([
            'id',
            'name',
            'country_code',
            'latitude',
            'longitude',
        ])->where(['country_id' => $country])->get();

//        $country->states;
        return Response::json($state);
    }

    public function country(int $country)
    {
        $country = Country::with(['states', 'cities'])
            ->find($country);
        return Response::json($country);
    }

    public function state(int $state)
    {
        $state = State::with(['country', 'cities'])->find($state);
        return Response::json($state);
    }

    public function city(City $city)
    {
        $city->country;
        $city->state;
        return Response::json($city);
    }
}
