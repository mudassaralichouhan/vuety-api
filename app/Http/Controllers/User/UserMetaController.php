<?php

namespace App\Http\Controllers\User;

use App\Helper\ImageManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordChangeRequest;
use App\Http\Requests\UserMetaRequest;
use App\Models\UserMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class UserMetaController extends Controller
{
    use ImageManager;

    public function show()
    {
        $user = Auth::id();
        return Response::json(UserMeta::where(['user_id' => $user])->first());
    }

    public function update(UserMetaRequest $request)
    {
        $user = Auth::user();
        $data = array_filter($request->all(), 'strlen');

        UserMeta::updateOrCreate([
            'user_id' => $user->id
        ], $data);

        return Response::json([
            'message' => 'Profile update successfully',
        ]);
    }

    public function passwordChange(PasswordChangeRequest $request)
    {
        $user = Auth::user();
        if (Hash::check($request->current_password, $user->password)) {
            $user->password = Hash::make($request->password);

            return Response::json([
                'message' => 'Password change successfully',
            ]);
        }

        return Response::json([
            'message' => 'your current password is incorrect the password was not changed',
        ], 403);
    }

    public function photo(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($file = $request->file('photo')) {

            $user = Auth::user();

            if ($user->photo)
                Storage::delete($user->photo);

            $filename = date('YmdHis') . '_' . md5(time()) . '.jpg';
            $user->photo = $file->storeAs('images/profile-photo', $filename);

            $user->save();

            return Response::json([
                'message' => 'image store successfully',
                'data' => route('storage.image', ['path' => $user->photo]),
            ]);
        }

        return Response::json([
            'message' => 'some thing was wrong',
        ], 403);
    }
}
