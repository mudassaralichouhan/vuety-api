<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryPhotosRequest;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class MediaController extends Controller
{
    public function index()
    {
        $photos = Media::paginate();

        return Response::json($photos);
    }

    public function show(int $photo_id)
    {
        $photo = Media::with(['albums'])->find($photo_id);
        return Response::json($photo);
    }

    public function store(GalleryPhotosRequest $request)
    {
        foreach ($request->file('files') as $file) {
            if ($file->getError() !== UPLOAD_ERR_OK) {
                return Response::json([
                    'message' => $file->getErrorMessage(),
                ], 500);
            }

            $filename = date('YmdHis') . '_' . md5(rand(11111, 999999)) . '.' . $file->getClientOriginalExtension();

            $meme = $file->getMimeType();
            $path = '';

            // Determine the file type and set the appropriate storage path
            if (strpos($meme, 'image') === 0) {
                $path = $file->storeAs('images/gallery-medias', $filename);
            } elseif (strpos($meme, 'audio') === 0) {
                $path = $file->storeAs('audio/gallery-medias', $filename);
            } elseif (strpos($meme, 'video') === 0) {
                $path = $file->storeAs('videos/gallery-medias', $filename);
            }

            if (empty($path)) {
                return Response::json([
                    'message' => 'Invalid file type',
                ], 400);
            }

            $photos[] = [
                'description' => $request->description,
                'file' => $path,
                'mime' => $meme,
                'favorite' => null,
                'status' => 1,
                'updated_at' => $date = date('Y-m-d H:i:s'),
                'created_at' => $date,
            ];
        }

        if (Media::insert($photos)) {
            return Response::json([
                'message' => 'Photos store successfully',
                'data' => '',
            ]);
        }

        return Response::json([
            'message' => 'some thing was wrong',
        ], 500);
    }

    public function attachWithFavorite(Request $request, int $photo)
    {
        $request->validate([
            'favorite' => [
                'required',
                Rule::in([0, 1]),
            ],
        ]);

        $photo = Media::find($photo);
        $photo?->update([
            'favorite' => $request->favorite == 1 ? 1 : null,
        ]);

        return Response::json([
            'message' => 'Photos save as favorite',
        ]);
    }

    public function destroy(int $photo)
    {
        if (Media::find($photo)?->delete())
            return Response::json([
                'message' => 'Photos delete successfully',
            ]);

        return Response::json([
            'message' => 'Photos not found',
        ], 404);
    }

    public function destroyForce(int $photo)
    {
        $photo = Media::onlyTrashed()->find($photo);
        if ($photo) {
            if ($photo->image['original'])
                Storage::delete($photo->image['original']);

            $photo->forceDelete();
            return Response::json([
                'message' => 'Gallery photo delete successfully',
            ]);
        }

        return Response::json([
            'message' => 'Gallery photo not found',
        ], 404);
    }

    public function restore(int $photo)
    {
        if (Media::onlyTrashed()->find($photo)?->restore())
            return Response::json(Media::find($photo));

        return Response::json([
            'message' => 'Gallery photo not found',
        ], 404);
    }

    public function trashList()
    {
        $photo = Media::onlyTrashed()->paginate();
        return Response::json($photo);
    }

    public function trashEmpty()
    {
        $photos = Media::select(['id', 'image'])->onlyTrashed();

        if ($photos->count()) {
            foreach ($photos->get() as $photo) {
                if ($photo->image['original'])
                    Storage::delete($photo->image['original']);
            }
            $photos->forceDelete();
        }

        return Response::json([
            'message' => 'Gallery photos delete successfully',
        ]);
    }
}
