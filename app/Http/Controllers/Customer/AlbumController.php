<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlbumRequest;
use App\Models\Album;
use App\Models\AlbumHasPhoto;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class AlbumController extends Controller
{
    public function index()
    {
        $auth = \Auth::user();

        $albums = $auth->albums()
            ->select(['id', 'title', 'paragraph', 'thumbnail', 'created_at'])
            ->paginate();

//        $galleries->contains(function ($value) {
//            //$value->thumbnail = route('storage.image', ['path' => $value->thumbnail]);
//        });

        return Response::json($albums);
    }

    public function show(int $album_id)
    {
        $auth = \Auth::user();

        $album = $auth->albums()->with(['photos'])
            ->find($album_id);
        return Response::json($album);
    }

    public function updateOrStore(AlbumRequest $request)
    {
        $user = \Auth::user();

        if (!$gallery = Album::where(['title' => $request->title])->first())
            $gallery = new Album();

        $gallery->title = $request->title;
        $gallery->paragraph = $request->paragraph;

        if ($file = $request->file('thumbnail')) {
            $filename = date('YmdHis') . '_' . md5(time()) . '.jpg';
            $filename = $file->storeAs('images/gallery-album-thumbnails', $filename);
            $thumbnail['thumbnail'] = $filename;
        } elseif ($request->input('image_url')) {
            $contents = file_get_contents($request->image_url);
            if (strlen($contents) < (1024 * 1024)) {
                $filename = 'images/gallery-album-thumbnails/' . date('YmdHis') . '_' . md5(time()) . '.jpg';
                if (Storage::put($filename, $contents)) {
                    $gallery->thumbnail = $filename;
                    $thumbnail['thumbnail'] = $filename;
                }
            }
        } elseif ($gallery->thumbnail) {
            $thumbnail['thumbnail'] = $gallery->thumbnail;
        }

        $album = $user->albums()->updateOrCreate(['title' => $request->title, 'user_id' => $user->id], [
            'title' => $request->title,
            'paragraph' => $request->paragraph,
            ...$thumbnail ?? throw new HttpResponseException(Response::json(['message' => ['Album image is required']], 422)),
        ]);

        return Response::json([
            'message' => 'Album store successfully',
            'data' => $album,
        ]);
    }

    public function destroy(int $id)
    {
        $user = \Auth::user();

        if ($albums = $user->albums()->select(['id', 'thumbnail'])->find($id)) {
            if ($albums->thumbnail['original'] ?? false) {
                //$url_components = parse_url($albums->thumbnail);
                //parse_str($url_components['query'], $params);
                //Storage::delete($params['path']);
                Storage::delete($albums->thumbnail['original']);
            }

            AlbumHasPhoto::where(['album_id' => $albums->id])->delete();
            $albums->forceDelete();

            return Response::json([
                'message' => 'Album delete successfully',
            ]);
        }

        return Response::json([
            'message' => 'Album not found',
        ], 404);
    }

    public function attachPhoto(Request $request, int $album_id)
    {
        $request->validate([
            'photo_ids' => 'required|array',
            'photo_ids.*' => 'exists:photos,id',
        ]);

        $user = \Auth::user();

        if ($user->albums()->exists($album_id)) {
            foreach ($request->photo_ids as $id) {
                AlbumHasPhoto::updateOrInsert([
                    'album_id' => $album_id,
                    'photo_id' => $id,
                ], [
                    'album_id' => $album_id,
                    'photo_id' => $id,
                ]);
            }

            return Response::json([
                'message' => 'Album photos store successfully',
            ]);
        }

        return Response::json([
            'message' => 'Album not found',
        ], 404);
    }

    public function removePhoto(int $album_id, int $photo_id)
    {
        $user = \Auth::user();

        if ($user->albums()->exists($album_id)) {
            AlbumHasPhoto::where([
                'album_id' => $album_id,
                'photo_id' => $photo_id,
            ])->delete();

            return Response::json([
                'message' => 'Album photos remove successfully',
            ]);
        }

        return Response::json([
            'message' => 'Album not found',
        ], 404);
    }
}
