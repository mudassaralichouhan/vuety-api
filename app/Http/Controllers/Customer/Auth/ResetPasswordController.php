<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseMessage;

class ResetPasswordController extends Controller
{
    public function forgot(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status === Password::RESET_THROTTLED)
            return Response::json([
                'message' => ['Too many request.']
            ], ResponseMessage::HTTP_UNAUTHORIZED);

        return Response::json([
            'message' => ['Forgot password link send to your email.'],
        ]);
    }

    public function reset(Request $request, string $token) {
        $credentials = $request->validate([
            'email' => 'email|max:255',
            'password' => 'required|max:25|string|confirmed'
        ]);
        $credentials['token'] = $token;

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::PASSWORD_RESET)
            return Response::json(['message' => ['Password has been successfully changed']]);

        if ($reset_password_status == Password::INVALID_USER)
            return Response::json(['message' => ['User not found']], ResponseMessage::HTTP_FORBIDDEN);

        return Response::json(['message' => ['Invalid token provided']], ResponseMessage::HTTP_FORBIDDEN);
    }

    public function redirectToWeb()
    {
        $query = http_build_query([
            'email' => $_GET['email'],
            'token' => $_GET['token'],
        ]);

        header('location: ' . env('WEB_BASE_URL') . '/password/reset?' . $query);
        exit;
    }
}
