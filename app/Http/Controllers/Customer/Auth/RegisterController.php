<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseMessage;

class RegisterController extends Controller
{
    const CUSTOMER_TOKEN = 'nfce_customer';

    public function register(RegisterRequest $request)
    {
        $user = User::create([
            ...$request->all(),
            ...['password' => Hash::make($request->password)]
        ]);

        $user->sendEmailVerificationNotification();

        $token = $user->createToken(self::CUSTOMER_TOKEN)->accessToken;

        return Response::json([
            'user' => $user,
            'token' => $token,
            'token_type' => 'Bearer ' . $token->token,
        ], ResponseMessage::HTTP_CREATED);
    }
}
