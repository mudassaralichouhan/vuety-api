<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseMessage;

class LoginController extends Controller
{
    public function auth()
    {
        return Response([
            'user' => Auth::user(),
        ],ResponseMessage::HTTP_OK);
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials)){

            $user = Auth::user();

            if ($user->email_verified_at) {
                return Response([
                    'user' => $user,
                    'token' => $user->createToken(RegisterController::CUSTOMER_TOKEN)->plainTextToken
                ], ResponseMessage::HTTP_OK);
            } else {

                Auth::logout();

                return Response::json([
                    'message' => ['You need to confirm your account.'],
                ], ResponseMessage::HTTP_FORBIDDEN);
            }
        }

        return Response(['message' => ['Bad creds email or password wrong']],ResponseMessage::HTTP_UNAUTHORIZED);
    }

    public function logout()
    {
        $user = Auth::user();
        $user->currentAccessToken()->delete();
        //Auth::logout();

        return Response::json(['message' => 'logout'], ResponseMessage::HTTP_OK);
    }
}
