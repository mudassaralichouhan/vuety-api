<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();

        $emails = Email::select(['id', 'email', 'email_verified_at', 'created_at'])
            ->where(['user_id' => $user_id])
            ->get();

        return Response::json($emails);
    }

    public function store(Request $request)
    {
        $user_id = Auth::id();

        $validator = Validator::make($request->all(), [
            'email' => "required|max:255|email|unique:users,email|unique:emails,email",
        ]);

        if ($validator->fails())
            return Response::json($validator->errors(), 422);

        $email = Email::updateOrCreate([
            'user_id' => $user_id,
            'email' => $request->email,
        ], [
            'email' => $request->email,
            'user_id' => $user_id,
        ]);

        return Response::json([
            'message' => 'E-mail store successfully',
            'data' => $email
        ]);
    }

    public function destroy(int $id)
    {
        Email::where(['id' => $id])->delete();

        return Response::json([
            'message' => 'E-mail delete successfully',
        ]);
    }

    public function sendVerificationTokenMakeSecondary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => "required|max:255|email|exists:emails,email",
        ]);

        if ($validator->fails())
            return Response::json($validator->errors(), 422);

        if ($email = Email::where(['email' => $request->email])->first()) {

            $result = hash_hmac('ripemd160', rand(999, 999999), '123456789');

            $url = route('email.make-secondary', $result);

            Mail::send('emails.email-verification-email', ['url' => $url], function ($message) use ($email) {
                $message->to($email->email);
                $message->subject('Secondary email verification');
            });

            $email->update([
                'verify_token' => $result,
                'email_verified_at' => null,
            ]);
        }

        return Response::json([
            'message' => ['Email verification link sent on your email ID']
        ]);
    }

    public function verifyTokenSecondary(string $token)
    {
        $token = Email::where([
            'verify_token' => $token,
        ])->first();

        if ($token) {
            $token->update([
                'email_verified_at' => date('Y-m-d H:i:s'),
                'verify_token' => null,
            ]);

            header('location: ' . env('WEB_BASE_URL') . '/email-secondary-verified?status=1');
            exit;
        }

        header('location: ' . env('WEB_BASE_URL') . '/email-secondary-verified?status=0');
        exit;
    }

    public function sendVerificationTokenMakePrimary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => "required|max:255|email|exists:emails,email",
        ]);

        if ($validator->fails())
            return Response::json($validator->errors(), 422);

        $user = Auth::user();
        $email = Email::select(['id', 'email'])
            ->where(['user_id' => $user->id, 'email' => $request->email])
            ->whereNotNull('email_verified_at')
            ->whereNull('verify_token')
            ->first();

        if ($user->email !== $request->email) {

            $result = hash_hmac('ripemd160', rand(999, 999999), '123456789');

            $url = route('email.make-primary', $result);

            Mail::send('emails.email-verification-email', ['url' => $url], function ($message) use ($email) {
                $message->to($email->email);
                $message->subject('Make Primary Verification');
            });

            $email->update([
                'verify_token' => $result,
            ]);
        }

        return Response::json([
            'message' => ['Email verification link sent on your email ID ' . $email->email]
        ]);
    }

    public function verifyTokenPrimary(string $token)
    {
        $email = Email::select(['id', 'email', 'user_id'])->where([
            'verify_token' => $token,
        ])
            ->whereNotNull('email_verified_at')
            ->first();

        $user = $email->user;

        if ($email && $user && $user->email !== $email->email) {

            $user->email = $email->email;
            $user->email_verified_at = date('Y-m-d H:i:s');

            if ($email->update() && $user->update()) {
                $email->delete();
                header('location: ' . env('WEB_BASE_URL') . '/email-primary-change-verified?status=1');
                exit;
            }
        }

        header('location: ' . env('WEB_BASE_URL') . '/email-primary-change-verified?status=0');
        exit;
    }
}
