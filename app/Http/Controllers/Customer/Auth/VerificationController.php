<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseMessage;

class VerificationController
{
    public function verified(User $user)
    {

        return view('auth.successfully-verify-email', [
            'user' => $user,
            'login_link' => env('WEB_BASE_URL') . '/login',
        ]);
//        return Response::json([
//            'message' => ['Email verified.'],
//            'data' => $user
//        ]);
    }

    public function verify($user_id, Request $request)
    {
        if (!$request->hasValidSignature()) {
            return Response::json([
                'message' => ['Invalid or Expired url provided.']
            ], ResponseMessage::HTTP_UNAUTHORIZED);
        }

        $user = User::find($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return Redirect::route('verification.verified', $user);
    }

    public function resend(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => "required|max:255|email",
        ]);

        if ($validator->fails())
            return Response::json($validator->errors(), 422);
        $user = User::where('email', $request->email)->first();

        if ($user?->hasVerifiedEmail()) {
            return Response::json([
                'message' => ['Email already verified.']
            ], ResponseMessage::HTTP_FORBIDDEN);
        }

        $user?->sendEmailVerificationNotification();

        return Response::json([
            'message' => ['Email verification link sent on your email ID']
        ]);
    }

    public function notice()
    {
        return Response::json([
            'message' => ['You need to confirm your account. We have sent you an activation link to your email.'],
        ], ResponseMessage::HTTP_FORBIDDEN);
    }
}
