<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->id();

            $table->string('description', 255)->nullable()->default(NULL);
            $table->string('file', 255)->unique();
            $table->string('thumbnail', 255)->nullable()->default(NULL);
            $table->tinyInteger('favorite', false, true)
                ->nullable()->default(NULL);

            $table->tinyInteger('status',)->default(1);
            $table->timestamp('deleted_at')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medias');
    }
};
