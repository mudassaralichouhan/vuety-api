<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('user_metas', function (Blueprint $table) {
            $table->id();

            $table->integer('user_id', false, true)->unique();
            $table->string('company_name')->nullable()->default(NULL);
            $table->string('address', 255)->nullable()->default(NULL);
            $table->integer('city_id', false, true)->unsigned()
                ->nullable()->default(NULL);
            $table->string('postal_code_id', 30)
                ->nullable()->default(NULL);
            $table->integer('state_id', false, true)
                ->nullable()->default(NULL);
            $table->integer('country_id', false, true)
                ->nullable()->default(NULL);
            $table->string('bio', 255)->nullable()->default(NULL);
            $table->string('about_me', 255)->nullable()->default(NULL);
            $table->string('website_url', 200)->nullable()->default(NULL);

            $table->tinyInteger('status', false, false)->default(1);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_metas');
    }
};
