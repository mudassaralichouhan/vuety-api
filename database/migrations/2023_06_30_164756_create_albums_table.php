<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->id();

            $table->integer('user_id', false, true);
            $table->string('title',100);
            $table->string('paragraph', 255)->nullable()->default(NULL);
            $table->string('thumbnail', 255)->nullable()->default(NULL);
            $table->tinyInteger('status',)->default(1);
            $table->timestamp('deleted_at')->nullable()->default(NULL);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('albums');
    }
};
